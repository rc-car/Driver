# Driver

This project houses the high level RC Car driver source code.

## Packages Needed
 - gcc
 - make
 - cmake

On debian bases systems with `apt` these packages can be installed by:

`sudo apt install gcc make cmake`

## Compiling & Running
```sh
# Clone the git repository
git clone git@gitlab.com:rc-car/Driver.git
cd Driver/

# Create build directory
mkdir build && cd build

# Set up CMake
cmake ..

# Compile
make # You might want to include the -j4 parameter for parallel compiling

# The executable is located in the project's /build/bin/ folder
cd bin/
./driver.out

# To clean up just remove the build folder in the project's root folder
cd ../..
rm -r build/

```
